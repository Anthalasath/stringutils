package stringutils

import "testing"

func TestEnsureHasSuffix(t *testing.T) {
	suffix := ".md"
	filename := "hello"
	inputs := []string{filename, filename + suffix}
	expected := filename + suffix

	for _, input := range inputs {
		result := EnsureHasSuffix(input, suffix)
		if result != expected {
			t.Errorf("Expected %s from %s but got %s", expected, input, result)
		}
	}
}

func TestFirstCharToUpper(t *testing.T) {
	cases := make(map[string]string)
	cases["hello"] = "Hello"
	cases["Hello"] = "Hello"
	cases["HELLO"] = "HELLO"

	for input, expected := range cases {
		result := FirstCharToUppper(input)
		if result != expected {
			t.Errorf("Expected %s from %s but got %s", expected, input, result)
		}
	}
}
