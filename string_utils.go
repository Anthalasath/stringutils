package stringutils

import (
	"strings"
	"unicode"
)

func EnsureHasSuffix(s string, suffix string) string {
	if strings.HasSuffix(s, suffix) {
		return s
	}
	return s + suffix
}

func FirstCharToUppper(s string) string {
	chars := make([]rune, len(s))
	for i, c := range s {
		if i == 0 {
			c = unicode.ToUpper(c)
		}
		chars[i] = c
	}
	return string(chars)
}
